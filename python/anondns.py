#!/usr/bin/python3
import requests
import random
import json
import sys


def realpub():
    rpubip = (requests.get('https://api.ipify.org/')).text
    return rpubip

def writedomain(hostname, apikey, ipaddr, path):
    jdns = {
        "subdomain": hostname,
        "tld": "anondns.net",
        "ip_registered": ipaddr,
        "ctl_token": apikey
    }
    print(jdns)
    domainlog = open(path, 'a+')
    domainlog.write(json.dumps(jdns))
    domainlog.close()
    return 0

def search(hostname):
    srchreq = 'https://anondns.net/api/search/name/'+hostname+'.anondns.net'
    rsearch = requests.get(srchreq)
    return rsearch

def register(hostname, ipaddr):
    regreq = 'https://anondns.net/api/register/'+hostname+'.anondns.net/a/'+ipaddr
    rregister = requests.get(regreq)
    return rregister

def update(hostname, apikey, ipaddr):
    upreq = 'https://anondns.net/api/set/name/'+hostname+'.anondns.net/'+apikey+'/a/'+ipaddr
    rupdate = (requests.get(upreq)).text
    return rupdate

def gensub(length=7):
    subdomain = ""
    for _ in range(0, length):
        subdomain += chr(random.randint(65, 90))
        subdomain += chr(random.randint(97, 122))

    return subdomain

def autorand():
    public = realpub()
    newsub = gensub()
    srch = search(newsub)
    
    try:
        if srch.json()["status"] == 1:
            print('Error: taken subdomain at', newsub)
            main()

    except KeyError:
        regd = register(newsub, public)
        apitoken = regd.json()["token"]
        return newsub, apitoken, public

# 2Do: Convert sys.argv to streamlined cli library
def main():
    if len(sys.argv) == 1:
        print('Error: try "--help"')
    
    if sys.argv[1] == '--help' or sys.argv[1] == '-h':
        print("anondns.py help:\n\t-a | --auto\tAutomatically register random subdomain with the current public IPv4 address.\n\t-u | --update\tUpdate a pre-existing subdomain\Syntax:\n\tanondns.py -u [Subdomain] [API Key]")
    elif sys.argv[1] == '--auto' or sys.argv[1] == '-a':
        (subdomain, apikey, pubv4) = autorand()
        print("Subdomain:\t" + subdomain + "\nAPI Token:\t" + apikey+ "\nIP Registered:\t" + pubv4)
    elif sys.argv[1] == '--update' or sys.argv[1] == '-u':
        update(sys.argv[2], sys.argv[3], realpub())
        print("Run to verify: dig " + sys.argv[2] + ".anondns.net")
    else:
        print('Error: try "--help"')
  
    exit(0)

if __name__ == "__main__":
    main()
