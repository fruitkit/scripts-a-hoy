#!/usr/bin/python3
## The path to preferred python interpreter, useful for running as a command line script.
import sys
import os


## Class declaration
class ScriptsAHoy:
    ## When instantiating an object of the class ScriptsAHoy.
    def __init__(self, first, last, mi=None):  ## These variables are required, unless default defined.
        self.first = first
        self.last = last
        self.mi = mi
        self.exList = [self.first, self.last, self.mi]
        self.counter = 0
        ## Self denotes an attribute of an object, may be unique to each object.
        ## All attributes unique to the object must be declared within __init__

    def smain(self):
        ## Try/except block example
        try:
            print('Scripts-A-Hoy! {0:b}'.format('Scripts-A-Hoy!'))
            ## The format function, allows for the following:
            ##      :b -- binary    || bin()
            ##      :x -- hex       || hex()
            ##      :o -- octal     || oct()
            ##      :d -- decimal   || int()
            ## Also see: ord() && char() for ASCII conversions.

            ## Statements or Loops -- if/while may contain the 'or' or 'and' keywords denoting || or &&
            ## See Also: is, contains, is not
            if self.mi == None:
                self.mi = input('Middle Initial:\t')
            elif self.mi == "J" or self.mi == "j":
                input('Is your middle initial \"James\"?')
            else:
                print('self.mi is not type: None or str: j/J')
            
            for item in self.exList:
                print('item:\t{0}'.format(item))

            while self.counter <= 10:
                print(self.counter)
            
            return True
            ## Return True for flawless execution.
            ## It is possible to return any type available within Python.

        except Exception as e:
            print('Error: {0}'.format(e))
            ## Print the Exception of e.
            return False
            ## Return False due to the error.

def main():
    """DOCSTRING: Default function."""
    pass
    ## pass is useful for outlining methods, without writing code.


if __name__ == "__main__":
    ecode = 7
    main()
    ## Normally if run from command line, __name__ will be __main__
    ## Thus, your main function is useful when creating such scripts/applications

    ## Using the sys library we can handle arguments
    print('argc: {0}'.format(len(sys.argv)))
    for argn in sys.argv:
            print('argv[{0}]: {1}'.format(argn, sys.argv[argn]))
    if len(sys.argv) == 3:
        sah = ScriptsAHoy(sys.argv[1], sys.argv[2], sys.argv[3])
        sah.smain()

    elif 3 > len(sys.argv) > 1:
        sah = ScriptsAHoy(sys.argv[1], sys.argv[2], 'GI')
        sah.smain()

    else:
        print('argv[0]: {0}'.format(sys.argv[0]))
        ## Will print only the script name, because no arguments were provided

    exit(ecode)
