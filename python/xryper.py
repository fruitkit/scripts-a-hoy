#!/usr/bin/python3
import sys
import os


def main(msg, otp):
    if len(msg) == len(otp):
        ct = []
        for i in range(0, len(msg)):
            ct.append(ord(msg[i]) ^ ord(otp[i]))
        
        ct_c = []
        for l in ct:
            ct_c.append(str(chr(l)))
        return ct, ct_c

    else:
        print("Encrypt:\n\txryper.py [message] [key]\n\tKey length must match message length.")
        exit(1)


def xor(data, key): 
    return bytes(a^b for a, b in zip(*map(bytes, [data.encode('utf8'), key.encode('utf8')]))) 

if __name__ == "__main__":
    if len(sys.argv) > 1:
        print("Key:", sys.argv[2], "\nPayload:", sys.argv[1])
        ciphertext_i, ciphertext_c = main(sys.argv[1], sys.argv[2])
        print("Encrypted:\t", ciphertext_c)
         
        deciphered = main(ciphertext_c, sys.argv[2])
        print("Decrypted:\t", deciphered[1])

        # sys.stdout.buffer.write())        
        
        exit(0)

    else:
        print("Encrypt:\n\txryper.py [message] [key]\n\tKey length must match message length.")
        exit(1)
