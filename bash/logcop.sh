#!/bin/bash
## scripts-a-hoy bash script to monitor the auth.log 
### STILL WIP

## Fresh looking shell for our script
clear
## Color Codes
red="\033[00;31m"
RED="\033[01;31m"
green="\033[00;32m"
GREEN="\033[01;32m"
brown="\033[00;33m"
YELLOW="\033[01;33m"
blue="\033[00;34m"
BLUE="\033[01;34m"
magenta="\033[00;35m"
MAGENTA="\033[01;35m"
cyan="\033[00;36m"
CYAN="\033[01;36m"
white="\033[00;37m"
WHITE="\033[01;37m"
## Sets No Color
NC="\033[00m"

#echo -e "${NC} I ${red}am ${cyan}Bash.${NC}"
## Reminder: End the statement with a ${NC} to ensure bash default color follows.

echo -e "${red}Checking privileges${NC}."
if [ "$EUID" -ne 0 ]
  then echo -e "Please run as ${blue}root${NC}!"
  exit
fi

if [[ -n $1 ]];
then
    AUTHLOG=$1
    echo Using Log file : $AUTHLOG
fi
LOG=/tmp/valid.$$.log
grep -v "invalid" $AUTHLOG > $LOG
users=$(grep "Failed password" $LOG | awk '{ print $(NF-5) }' | sort | uniq)
printf "%-5s|%-10s|%-10s|%-13s|%-33s|%s\n" "Sr#" "User" "Attempts" "IP address" "Host_Mapping" "Time range"
ucount=0;
ip_list="$(egrep -o "[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+" $LOG | sort | uniq)"
for ip in $ip_list;
do
    grep $ip $LOG > /tmp/temp.$$.log
    for user in $users;
    do
        grep $user /tmp/temp.$$.log> /tmp/$$.log
        cut -c-16 /tmp/$$.log > $$.time
        tstart=$(head -1 $$.time);
        start=$(date -d "$tstart" "+%s");
        tend=$(tail -1 $$.time);
        end=$(date -d "$tend" "+%s")
        limit=$(( $end - $start ))
        if [ $limit -gt 120 ];
        then
            let ucount++;
            IP=$(egrep -o "[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+" /tmp/$$.log | head -1 );
            TIME_RANGE="$tstart-->$tend"
            ATTEMPTS=$(cat /tmp/$$.log|wc -l);
            HOST=$(host $IP | awk '{ print $NF }' )
            printf "%-5s|%-10s|%-10s|%-10s|%-33s|%-s\n" "$ucount" "$user" "$ATTEMPTS" "$IP" "$HOST" "$TIME_RANGE";
        fi
    done
done
rm /tmp/valid.$$.log /tmp/$$.log $$.time /tmp/temp.$$.log 2> /dev/null

exit 0
