#!/bin/bash -ex
## scripts-a-hoy bash script to output current user statistics

## Fresh looking shell for our script
clear
## Color Codes
red="\033[00;31m"
RED="\033[01;31m"
green="\033[00;32m"
GREEN="\033[01;32m"
brown="\033[00;33m"
YELLOW="\033[01;33m"
blue="\033[00;34m"
BLUE="\033[01;34m"
magenta="\033[00;35m"
MAGENTA="\033[01;35m"
cyan="\033[00;36m"
CYAN="\033[01;36m"
white="\033[00;37m"
WHITE="\033[01;37m"
## Sets No Color
NC="\033[00m"

#echo -e "${NC} I ${red}am ${cyan}Bash.${NC}"
## Reminder: End the statement with a ${NC} to ensure bash default color follows.

echo -e "${red}Checking privileges${NC}."
if [ "$EUID" -ne 0 ]
  then echo -e "Please run as ${red}root${NC}!"
  exit 1
fi

log=/var/log/wtmp
if [[ -n $1 ]];
then
    log=$1
fi
printf "%-4s %-10s %-10s %-6s %-8s\n" "Rank" "User" "Start" "Logins" "Usage hours"
last -f $log | head -n -2 > /tmp/ulog.$$
cat /tmp/ulog.$$ | cut -d' ' -f1 | sort | uniq> /tmp/users.$$
(
while read user;
do
    grep ^$user /tmp/ulog.$$ > /tmp/user.$$
    minutes=0
    while read t
    do
        s=$(echo $t | awk -F: '{ print ($1 * 60) + $2 }')
        let minutes=minutes+s
    done< <(cat /tmp/user.$$ | awk '{ print $NF }' | tr -d ')(')
    firstlog=$(tail -n 1 /tmp/user.$$ | awk '{ print $5,$6 }')
    nlogins=$(cat /tmp/user.$$ | wc -l)
    hours=$(echo "$minutes / 60.0" | bc)
    printf "%-10s %-10s %-6s %-8s\n" $user "$firstlog" $nlogins $hours
done< /tmp/users.$$
) | sort -nrk 4 | awk '{ printf("%-4s %s\n", NR, $0) }'
rm /tmp/users.$$ /tmp/user.$$ /tmp/ulog.$$

exit 0
