#!/bin/bash

clear

echo -e "${red}Checking privileges${NC}."
if [ "$EUID" -ne 0 ]
  then echo -e "Please run as ${red}root${NC}!"
  exit
fi

# Random sleep timer between "0h 30m 1s" to "5h 59m 59s" not in use
sleep $((0.5 + RANDOM % 5))h $((0 + RANDOM % 59))m $((0 + RANDOM % 59))s
