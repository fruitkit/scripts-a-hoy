#!/bin/bash
## scripts-a-hoy bash script to ping live hosts on LAN
### STILL WIP

## Fresh looking shell for our script
clear
## Color Codes
red="\033[00;31m"
RED="\033[01;31m"
green="\033[00;32m"
GREEN="\033[01;32m"
brown="\033[00;33m"
YELLOW="\033[01;33m"
blue="\033[00;34m"
BLUE="\033[01;34m"
magenta="\033[00;35m"
MAGENTA="\033[01;35m"
cyan="\033[00;36m"
CYAN="\033[01;36m"
white="\033[00;37m"
WHITE="\033[01;37m"
## Sets No Color
NC="\033[00m"

echo -e "${red}Checking privileges${NC}."
if [ "$EUID" -ne 0 ]
  then echo -e "Please run as ${red}root${NC}!"
  exit 1
fi

for ip in $1{1..255} ;
do
    ping $ip -c 2 &> /dev/null ;

    if [ $? -eq 0 ];
    then
        echo -e "$ip is ${green}alive${NC}!"
    fi
done

exit 0