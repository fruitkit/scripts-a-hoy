#!/bin/bash -ex
## scripts-a-hoy bash script to share networking between interfaces

## Fresh looking shell for our script
clear
## Color Codes
red="\033[00;31m"
RED="\033[01;31m"
green="\033[00;32m"
GREEN="\033[01;32m"
brown="\033[00;33m"
YELLOW="\033[01;33m"
blue="\033[00;34m"
BLUE="\033[01;34m"
magenta="\033[00;35m"
MAGENTA="\033[01;35m"
cyan="\033[00;36m"
CYAN="\033[01;36m"
white="\033[00;37m"
WHITE="\033[01;37m"
## Sets No Color
WHITE="\033[01;37m"
## Sets No Color
NC="\033[00m"

#echo -e "${NC} I ${red}am ${cyan}Bash.${NC}" --ctstate NEW -j ACCEPT
iptables -A FORWARD -m conntrack --ctstate ESTABLISHED,RELATED -j ACCEPT
iptables -A POSTROUTING -t nat -j MASQUERADE

exit 0
