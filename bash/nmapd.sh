#!/bin/bash -ex
# USAGE: nmapd.sh <IP ADDRESS/CIDR> <LOGGING DIRECTORY>
# <IP ADDRESS/CIDR>:    Argument Required
# <LOGGING DIRECTORY>:  Argument Optional


# Check execution permissions
if [[ $EUID -ne 0 ]]; then
	>&2 echo "This script must be run as root"
	exit 1
fi

unalias -a

# Argument Variables
SCANSTAMP="$(/usr/bin/date +'%d%b%Y-%H%M%S%Z'| tr '[:lower:]' '[:upper:]')"

if [[ "$#" -eq "1" ]]; then
	SCANOUTDIR="$HOME/.nmapd"
else
	SCANOUTDIR="$2"
fi

if [[ "$#" -eq "0" ]]; then
	echo "Error: Missing Argument <IP Address/CIDR>"
	exit 1
fi

nmap -sn $1 | awk '/Nmap scan report/{print $NF}' > $SCANOUTDIR/alives-$SCANSTAMP.txt

# Check for logging folder
[ -d $SCANOUTDIR ] || mkdir -pv $SCANOUTDIR

#masscan -p1-65535,U:1-65535 $1 --rate=10000  --open-only -oX $SCANOUTDIR/masscan-$SCANSTAMP.xml

nmap -p- -sS -T4 -A -oA $SCANOUTDIR/nmap-scan-$SCANSTAMP $1

exit 0

