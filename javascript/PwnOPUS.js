// init global variables
const token = sessionStorage.getItem('OPUS');
const payload = JSON.parse(utils.decodeJWTPayload(token));
var courseID = sessionStorage.getItem('OPUS_trainee_course');
var answerDB = [];
var debug = true;
// Util fx's
function sleep(ms) {
	return new Promise(resolve => setTimeout(resolve, ms));
}

/*
Meat & Potatoes 
*/

// Send ajax request to get test data
var ajaxReq = $.ajax({ 
	url: '/api/courses/detail/' + courseID + '/1/1',
	method: 'GET',
	headers: {
		Authorization: token
	},
	success: function(resp) {
		if(resp && resp.success) {
			var questions = resp.data.questions; // This is the key
			for (var i = 0; i < questions.length; i++) {
				var opts = (questions[i]).questionOptions;
				var qText = (questions[i]).questionText;
				for (var j = 0; j < opts.length; j++) {
					if (opts[j].correctAnswerInd == 1) {
						var aText = opts[j].optionText;
						if (debug) {
							console.log("Question:", "\t", qText);
							console.log("Answer:", "\t", aText);
						}
						answerDB.push(opts[j]);
					}
					else if (opts[j].correctAnswerInd == "1") {	
						var aText = opts[j].optionText;
						if (debug) {
							console.log("Question:", "\t", qText);
							console.log("Answer:", "\t", aText);
						}
						answerDB.push(opts[j]);
					}
				}
			}
		}
	},
	fail: function(resp) {
		console.log("Oops something failed in ajax req.");
	}
});
await sleep(2000);

// Get all radio buttons 
var radioObjects = $('input[type=radio]').map((idx, question) => {
	return $(question);
});
var radios = ($('input[type=radio]').map((idx, question) => {
	return $(question).data();
})).toArray();

// Then set the correct ones from answerDB to true
for (var a = 0; a < answerDB.length; a++) {
	for (var r = 0; r < radios.length; r++) {
		if ((radios[r].optionId == answerDB[a].id) && (radios[r].questionId == answerDB[a].questionID)) {
			if (debug) { 
				console.log("Found a match for:", "\"" + answerDB[a].optionText + "\"", "radio button:", radios[r], "answerDB answerID:", answerDB[a].id, "answerDB questionID:", answerDB[a].questionID);  
			}
			((radioObjects[r])[0]).checked = true;
		}
	}
}