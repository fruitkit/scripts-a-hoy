# src: https://blogs.technet.microsoft.com/heyscriptingguy/2012/07/02/use-powershell-for-network-host-and-port-discovery-sweeps/

# (Nmap.exe)
# & 'C:Nmapnmap.exe' -F 10.0.0.1/24 -oX C:tempnmap.xml
	# & -- execution character
	# argv[1] -- path/executable
	# argv[2] -- arguments for argv[1]



# src: https://docs.microsoft.com/en-us/powershell/module/microsoft.powershell.utility/invoke-restmethod?view=powershell-6#description
(Invoke-WebRequest -Uri "https://api.ipify.org/?=json")["ip"]  #==> should return pubipv4 msDelay)
"10.0.0.2","10.0.0.3" | ForEach-Object { $ping.Send($_, 500) }
# [Write response to file on disk]
# api.ipify.org also allows: ?format=json || jsonp, &&  callback=getip
$Response = Invoke-WebRequest -Uri 'https://api.ipify.org'
$Stream = [System.IO.StreamWriter]::new('./pubip.txt', $false, $Response.Encoding)
try {
    $Stream.Write($Response.Content)
}
finally {
    $Stream.Dispose()
}
# Invoke-RestMethod
$ip = Invoke-RestMethod -Uri 'https://api.ipify.org?format=json'
"My public IP address is: $($ip.ip)"


# src: https://docs.microsoft.com/en-us/powershell/module/microsoft.powershell.utility/new-object?view=powershell-6
# [IE COM Object]
$IE = New-Object -COMObject InternetExplorer.Application -Property @{Navigate2="www.microsoft.com"; Visible = $True}
# [COM Object for Win32 Desktop]
$objshell = New-Object -COMObject "Shell.Application"
$objshell | Get-Member
# Returns list of things to interact with Win32 Desktop features

# src: https://blogs.technet.microsoft.com/heyscriptingguy/2013/06/25/use-powershell-to-interact-with-the-windows-api-part-1/
# src: http://www.exploit-monday.com/2012/03/powershell-live-memory-analysis-tools.html
# src: https://github.com/mattifestation/PSReflect/blob/master/PSReflect.psm1#L37
# src: https://github.com/PowerShellMafia/PowerSploit/blob/master/Exfiltration/Out-Minidump.ps1

#############################################################################
# Screen Shooter
#############################################################################
$File = "./bullet-" + Get-Random.toString() + ".gif"
Add-Type -AssemblyName System.Windows.Forms
Add-type -AssemblyName System.Drawing
# Gather Screen resolution information
$Screen = [System.Windows.Forms.SystemInformation]::VirtualScreen
$Width = $Screen.Width
$Height = $Screen.Height
$Left = $Screen.Left
$Top = $Screen.Top
# Create bitmap using the top-left and bottom-right bounds
$bitmap = New-Object System.Drawing.Bitmap $Width, $Height
# Create Graphics object
$graphic = [System.Drawing.Graphics]::FromImage($bitmap)
# Capture screen
$graphic.CopyFromScreen($Left, $Top, 0, 0, $bitmap.Size)
# Save to file
$bitmap.Save($File, System.Drawing.Imaging.ImageFormat.Gif) 
Write-Output "Screenshot saved to:"
Write-Output $File





#############################################################################
# TCP Client
#############################################################################
# Define Target
$Remote.Host = '192.168.0.1'
$Remote.Port = "80"

# Type Declaration, Var = Type Declaration::new() <== as in new object specifying 
[System.Net.Sockets.TcpClient] $tcpHandshake = [System.Net.Sockets.TcpClient]::new($Remote.Host, $Remote.Port)
# $tcpHandshake = New-Object System.Net.Sockets.TcpClient($Remote.Host, $Remote.Port)


# TCP Objects Stream (stdout)
$tcpStream = $tcpHandshake.GetStream()

if ($tcpHandshake) {
	"Hearty Handshake! ==> " + $Remote.Host + ":" + $Remote.Port.toString()
}

# File System Read/Writer's
# Note, the $tcpStream is being passed to StreamReader() and StreamWriter()
$reader = New-Object System.IO.StreamReader($tcpStream)
$writer = New-Object System.IO.StreamWriter($tcpStream)
# [System.IO.StreamReader] $reader = [System.IO.StreamReader]::new($tcpStream)
# [System.IO.StreamWriter] $writer = [System.IO.StreamWriter]::new($tcpStream)
$writer.AutoFlush = $true

$buffer = New-Object System.Byte[] 1024
# above Type: System.Array(Byte[])
# 1kb Type: System.ValueType(Int32) 
$encoding = New-Object System.Text.AsciiEncoding 

while ($tcpHandshake.Connected)
{
    while ($tcpStream.DataAvailable)
    {

        $rawresponse = $reader.Read($buffer, 0, 1024)
        $response = $encoding.GetString($buffer, 0, $rawresponse)   
    }

    if ($tcpHandshake.Connected)
    {
        Write-Host -NoNewline "<^-^> $ "
        $command = Read-Host

        if ($command -eq "exit")
        {
            break
        }

        $writer.WriteLine($command) | Out-Null
    }
    start-sleep -Milliseconds 500
}

$reader.Close()
$writer.Close()
$tcpHandshake.Close()
