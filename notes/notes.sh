
# Git configurations with VSCode
    # do once
        git config --global user.email "dev@developer-workstation"
    # for each repo
        git clone git@bitbucket.org:fruitkit/scripts-a-hoy.git 
        # git clone https://fruitkit@bitbucket.org/fruitkit/scripts-a-hoy.git
        git remote set-url origin git@bitbucket.org:fruitkit/scripts-a-hoy.git
    # for each session
        git push origin nightly


# wget download to specified directory
    wget https://httpbin.org/sample.tar.gz -P /tmp/tmpdir/

# Linking Hard/Symbolic
    ln [src file] [dest link]       #==> creates a hardlink
    ln -s [src file] [dest link]    #==> creates a symlink

# Bootable USB
    dd if=/home/user/kali-linux-1.0.4-i386.iso of=/dev/sdc1 bs=512M; sync

# OpenSSL tar archives
    tar -czf - * | openssl enc -e -aes256 -out secured.tar.gz
    openssl enc -d -aes256 -in secured.tar.gz | tar xz -C test
        # output the decrypted tar file to a folder "test"
# OpenSSL single file
    openssl enc -aes-256-cbc -in ~/Documents/test.txt -out ~/Documents/test.dat
    openssl enc -aes-256-cbc -d -in ~/Documents/test.dat > ~/Documents/test.txt

# Encrypt/Decrypt file (gpg default: AES-128 algorithm)
    gpg -c ~/Documents/test.txt
    gpg ~/Documents/test.txt.gpg

    bcrypt ~/Documents/test.txt
    bcrypt ~/Documents/test.txt.bfe
    # bcrypt algorithm is deprecated

    # ccrypt uses AES
    ccencrypt ~/Documents/test.txt
    ccdecrypt ~/Documents/test.txt.cpt

    # Zip archives
    zip --password myP@$Sw()rd^ zabc.zip a.txt b.txt c.txt
    unzip zabc.zip

    # 7zip archives
    7za a -tzip -p -mem=AES256 7zabc.zip a.txt b.txt c.txt
    7za e 7zabc.zip
