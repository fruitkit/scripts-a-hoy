# Src: http://www.exploit-monday.com/2012/03/powershell-live-memory-analysis-tools.html
# Author: Matt Graeber

function Check-MemoryProtection {


   [CmdletBinding()] Param (
        [Parameter(Position = 0, Mandatory = $True)] [System.IntPtr] $Address,
        [Parameter(Position = 1)] [Int] $ProcessId,
        [Parameter(Position = 2)] [Int] $PageSize = 0x1000
    )
    
    try{$mem = New-Object Winapi.Kernel32+MEMORY_BASIC_INFORMATION}
    catch
    {
        $code = @"
        using System;
        using System.Runtime.InteropServices;

        namespace Winapi
        {
            public class Kernel32
            {
                [Flags]
                public enum ProcessAccessFlags : uint
                {
                    PROCESS_VM_READ = 0x00000010,
                    PROCESS_QUERY_INFORMATION = 0x00000400,
                    ALL = 0x001F0FFF
                }
            
                [Flags]
                public enum AllocationProtectEnum : uint
                {
                    PAGE_EXECUTE = 0x00000010,
                    PAGE_EXECUTE_READ = 0x00000020,
                    PAGE_EXECUTE_READWRITE = 0x00000040,
                    PAGE_EXECUTE_WRITECOPY = 0x00000080,
                    PAGE_NOACCESS = 0x00000001,
                    PAGE_READONLY = 0x00000002,
                    PAGE_READWRITE = 0x00000004,
                    PAGE_WRITECOPY = 0x00000008,
                    PAGE_GUARD = 0x00000100,
                    PAGE_NOCACHE = 0x00000200,
                    PAGE_WRITECOMBINE = 0x00000400,
                }
                
                [Flags]
                public enum StateEnum : uint
                {
                    MEM_COMMIT = 0x00001000,
                    MEM_FREE = 0x00010000,
                    MEM_RESERVE = 0x00002000,
                }
                
                [Flags]
                public enum TypeEnum : uint
                {
                    MEM_IMAGE = 0x01000000,
                    MEM_MAPPED = 0x00040000,
                    MEM_PRIVATE = 0x00020000,
                }
            
                [StructLayout(LayoutKind.Sequential)]
                public struct MEMORY_BASIC_INFORMATION
                {
                    public UIntPtr BaseAddress;
                    public UIntPtr AllocationBase;
                    public AllocationProtectEnum AllocationProtect;
                    public IntPtr RegionSize;
                    public StateEnum State;
                    public AllocationProtectEnum Protect;
                    public TypeEnum Type;
                }

                [DllImport("kernel32.dll")]
                public static extern IntPtr OpenProcess(ProcessAccessFlags dwDesiredAccess, [MarshalAs(UnmanagedType.Bool)] bool bInheritHandle, uint dwProcessId);
                [DllImport("kernel32.dll")]
                public static extern int VirtualQuery(IntPtr lpAddress, ref MEMORY_BASIC_INFORMATION lpBuffer, int dwLength);
                [DllImport("kernel32.dll")]
                public static extern int VirtualQueryEx(IntPtr hProcess, IntPtr lpAddress, ref MEMORY_BASIC_INFORMATION lpBuffer, int dwLength);
                [DllImport("kernel32.dll")]
                public static extern bool ReadProcessMemory(IntPtr hProcess, IntPtr lpBaseAddress, [Out, MarshalAs(UnmanagedType.AsAny)] object lpBuffer, int dwSize, [Out] int lpNumberOfBytesRead);
                [DllImport("kernel32.dll")]
                public static extern bool CloseHandle(IntPtr hObject);
            }
        }
"@

        $codeProvider = New-Object Microsoft.CSharp.CSharpCodeProvider
        $location = [PsObject].Assembly.Location
        $compileParams = New-Object System.CodeDom.Compiler.CompilerParameters
        $assemblyRange = @("System.dll", $location)
        $compileParams.ReferencedAssemblies.AddRange($assemblyRange)
        $compileParams.GenerateInMemory = $True
        $codeProvider.CompileAssemblyFromSource($compileParams, $code) | Out-Null
    }

    $mem = New-Object Winapi.Kernel32+MEMORY_BASIC_INFORMATION

    if ($ProcessId)
    {
        $ProcHandle = [Winapi.Kernel32]::OpenProcess([Winapi.Kernel32+ProcessAccessFlags]::PROCESS_QUERY_INFORMATION, 0, $ProcessId)
        [Winapi.Kernel32]::VirtualQueryEx($ProcHandle, $Address, [ref] $mem, $PageSize) | Out-Null
        [Winapi.Kernel32]::CloseHandle($ProcHandle) | Out-Null
    }
    else
    {
        [Winapi.Kernel32]::VirtualQuery($Address, [ref] $mem, $PageSize) | Out-Null
    }
    
    return $mem
}

function Memory-Reader {
    [CmdletBinding()] Param (
        [Parameter(Position = 1, Mandatory = $True)] [System.IntPtr] $Address,
        [Parameter(Position = 1, Mandatory = $True)] [Int] $Offset,
        [Parameter()] [Int] $ProcessId,
        [Parameter()] [Int] $Width = 16,
        [Parameter()] [String] $DumpToFile,
        [Parameter()] [bool] $StringsOnly,       
        [Parameter()] [String] $Encoding = 'DEFAULT',
        [Parameter()] [Int] $MinimumLength = 3,
        [Parameter()] [Switch] $StringOffset
    )
    # Address = Base address to dump
    # Offset = Total bytes to dump // Hex format - 0x98
    # Width = Text formatting - output width
    # DumpToFile = If not specified - memory is dumped to stdout

    # Change the address from IntPtr to an Int64
    $BaseAddress = $Address.ToInt64()

    # Increment through the processes memory page
    for ($PageOffset = 0; $PageOffset -lt $Offset; $PageOffset += 0x1000)
    {
        # Cast to IntPtr
        $PageBaseAddress = [IntPtr]($BaseAddress + $PageOffset)

        # Supply PageBaseAddress & ProcessID to Check-MemoryProtection || else
        if ($ProcessId) {$MemProtect = Check-MemoryProtection $PageBaseAddress $ProcessId}
        else {$MemProtect = Check-MemoryProtection $PageBaseAddress}
        # Returns process memory check as $MemProtect

        # If memory has PAGE_NOACCESS ==> error out: inaccessible
        if ($MemProtect.Protect -eq [Winapi.Kernel32+AllocationProtectEnum]::PAGE_NOACCESS)
        {throw "Memory region at base address 0x$($PageBaseAddress.ToString('X16')) is inaccessible!`n `nMemory Protection Information:`n$($MemProtect | Out-String)`n `n"}
    }
    # End for loop & memory page access 

    # This is where process memory will be coppied
    # Type cast to Byte[]
    # Byte[] of the size Offset!
    [Byte[]] $ByteArray = New-Object Byte[]($Offset)

    # Create Process handle, read the process to ByteArray
    if ($ProcessId) {
        $BytesRead = 0

        # From Kernel32 ==> OpenProcess()
        # Add arg[0] as Kernel32's ProcessAccessFlags, add ProcessId
        $ProcHandle = [Winapi.Kernel32]::OpenProcess(([Winapi.Kernel32+ProcessAccessFlags]::PROCESS_VM_READ), 0, $ProcessId)

        # ReadProcessMemory of the process-handle, output bytearray, 
        [Winapi.Kernel32]::ReadProcessMemory($ProcHandle, $Address, $ByteArray, $Offset, $BytesRead) | Out-Null

        # Closes ProcHandle
        [Winapi.Kernel32]::CloseHandle($ProcHandle) | Out-Null
    }
    else {
        # Straight copy all of Memory from the Address to Offset
        [System.Runtime.InteropServices.Marshal]::Copy($Address, $ByteArray, 0, $Offset)
    }
    $Position = 0
    $Padding = ($Width * 2) + $Width

    # Dumps only to a file
    if($DumpToFile){
        # Verify path
        if ($FilePath = Split-Path $DumpToFile){
            if (Test-Path $FilePath){$File = "$(Resolve-Path $FilePath)\$DumpToFile"}
            else{throw "Invalid file path!"}
        }# No filepath provided, give absolute path to current directory
        else{$File = "$(Resolve-Path .)\$DumpToFile"}
        
        # Create a new stream, for file output
        $Stream = New-Object System.IO.FileStream($File, [System.IO.FileMode]::Create)
        # Write to ByteArray until Offset // ByteArray's size should be the size of Offset provided
        $Stream.Write($ByteArray, 0, $Offset)
        $Stream.Close()
    }
    # Print out to console rather than file dump, if StringsOnly isnt specified
    elseif(!$DumpStrings -and $DumpToFile){
        # Position is default 0
        # This will run if Offset != 0 // Keeps
        while ($Position -le ($Offset-1)){
            $Line = ""

            # Position 0 to string 8 long in hex format
            $Line = "$($Position.ToString('X8'))h  "
            $PrintBytes = ""
            $Text = ""

            foreach ($i in 0..($Width-1))
            {
                if ($Position -ge $Offset) {break}

                # toPrint <== ByteArray[Position]
                $PrintBytes += "$($ByteArray[$Position].ToString('X2')) "
                
                # If printable keep char, else print '.'
                if ( [Char]::IsLetterOrDigit($ByteArray[$Position]) -or [Char]::IsPunctuation($ByteArray[$Position]) -or [Char]::IsSymbol($ByteArray[$Position]) ){$Text += [Char] $ByteArray[$Position]}
                else{$Text += '.'}
                # Increment byte position
                $Position++
            }
            $Line += $PrintBytes.PadRight($Padding, ' ')
            $Line += " $Text"
            # Output Line
            $Line
        }
    }
    elseif ($StringsOnly -and !$DumpToFile){
        if ($Encoding.ToUpper() -eq 'DEFAULT') {
        $ArrayPtr = [System.Runtime.InteropServices.Marshal]::UnsafeAddrOfPinnedArrayElement($ByteArray, 0)
        $RawString = [System.Runtime.InteropServices.Marshal]::PtrToStringAnsi($ArrayPtr, $ByteArray.Length)
        $Regex = [regex] "[\x20-\x7E]{$MinimumLength,}"
        $Results = $Regex.Matches($RawString)
        # Unicode Regex
        $Encoder = New-Object System.Text.UnicodeEncoding
        $RawString = $Encoder.GetString($ByteArray, 0, $Offset)
        $Regex = [regex] "[\u0020-\u007E]{$MinimumLength,}"
        $Results += $Regex.Matches($RawString)
        }
        elseif ($Encoding.ToUpper() -eq 'ASCII') {
            # This hack will get the raw ascii chars. The System.Text.UnicodeEncoding object will replace some unprintable chars with question marks.
            $ArrayPtr = [System.Runtime.InteropServices.Marshal]::UnsafeAddrOfPinnedArrayElement($ByteArray, 0)
            $RawString = [System.Runtime.InteropServices.Marshal]::PtrToStringAnsi($ArrayPtr, $ByteArray.Length)
            $Regex = [regex] "[\x20-\x7E]{$MinimumLength,}"
            $Results = $Regex.Matches($RawString)
        }
        else {
            # Unicode Regex
            $Encoder = New-Object System.Text.UnicodeEncoding
            $RawString = $Encoder.GetString($ByteArray, 0, $Offset)
            $Regex = [regex] "[\u0020-\u007E]{$MinimumLength,}"
            $Results = $Regex.Matches($RawString)
        }
        
        if ($StringOffset) {
            $Results | ForEach-Object { "$($_.Index):$($_.Value)" }
        }
        else {
            $Results | ForEach-Object { "$($_.Value)" }
        }
    }
}

