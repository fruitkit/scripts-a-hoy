# Example Targets
$targs = {
    $targs.hosts: ["192.168.0.1", "192.168.0.7"],
    $targs.port: "445",
    $targs.psz: 420,

}

# Ping sweeper
$ping = New-Object System.Net.Networkinformation.ping

# TCP socket
$sockstream = New-Object System.Net.Sockets.TCPClient

0..($targs.hosts | Measure-Object).length.Maximum |% {
    # Ping Echo Request
    $ping.Send($targs.hosts[$_], $targs.psz)

    # DNS lookup
    [Net.DNS]::GetHostEntry($targs.hosts[$_])

    # TCP Connect
    $sockstream.Connect($targs.hosts[$_], $targs.port)
    # Test Connection Bool
    if ($sockstream.Connected) 
    {
        Write-Host "Connected: " + $targs.hosts[$_] + ":" + $targs.port
    }
    # TCP Teardown
    $sockstream.Close()
}

