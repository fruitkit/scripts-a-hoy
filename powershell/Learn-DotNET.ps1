<#
######################################################
#####################  NOTE.D  #######################
######################################################
# [Reflection.Assembly]::LoadFile("c:\temp\MyMathLib.dll")
# Add-Type -path './temp/MyMathLib.dll'
# [System.Reflection.Assembly]::LoadWithPartialName("Microsoft.VisualBasic")
# System.Threading.Thread.Sleep(1000)
# $WER = [PSObject].Assembly.GetType('System.Management.Automation.WindowsErrorReporting')
# 	 $WER | gm | Where-Object -Property MemberType -EQ Method
# [PSObject].Assembly.GetTypes()
# Lists Assemlies
######### https://github.com/PowerShellMafia/PowerSploit/blob/master/CodeExecution/Invoke-ReflectivePEInjection.ps1 #########
#>

<#
######################################################
###################### NET DEF #######################
######################################################
(Class) IO.MemoryMappedFiles:   
	MemoryMappedFile.CreateFromFile(): Load pointer to file on disk and "map" it
		With above object ==> CreateViewAccessor(offset=256MB, length=512MB)
			offset ~ 256mb || 0x10000000 ==> file start @ 256mb of mapped file on disk
			length ~ (512mb || 0x20000000) + offset = 768MB ==> end read byte/total view size
			accessorObj.Read(int)
			accessorObj.Write(int)
(Class) 

#>
<#
######################################################
####################### TO DO ########################
######################################################
# io ==> https://docs.microsoft.com/en-us/dotnet/api/system.io?view=netframework-4.7.2
# io.memorystream ==> https://docs.microsoft.com/en-us/dotnet/api/system.io.memorystream?view=netframework-4.7.2
# io.memorymappedfile ==> https://docs.microsoft.com/en-us/dotnet/api/system.io.memorymappedfiles.memorymappedfile?view=netframework-4.7.2

#>

# START
# $BufferObj = New-Object System.Buffer
# SrcAddr       being starting address of whats coppied
# DstAddr       being the place to copy these bytes to
# DstBufferSz   (Int64/Uint64)  being the max size of destination buffer
# SrcBytes      (Int64/Uint64)  being the number of bytes to copy to DstAddr++
# $BufferObj.MemoryCopy($SrcAddr, $DstAddr, $DstBufferSz, $SrcBytes)
# END



# START
# Add-Type -AsseblyName System.Reflection
# Add-Type -AsseblyName System.AppDomain
# $mathersRefObj = New-Object System.Runtime.InteropServices.Marshal.MarshalByRefObject
# END



# # Args: new(object), Ret: IntPtr
# $pidy = 6969420
# Add-Type -AsseblyName System.Runtime.InteropServices
# Add-Type -path 'mscorlib.dll'
# Add-Type -path 'netstandard.dll'
# # $pidy is placeholder for the actual process; $pid is the powershell pid
# Write-Host 'Calling Marshal.GetIUnknownForObject'
# $pidyPointer = New-Object System.Runtime.InteropServices.Marshal.GetIUnknownForObject($pidy)
# # Returns an IntPtr into $pidyPointer
# Write-Host $pidy, $pidyPointer
# Write-Host 'Calling Marshal.Release'
# $pidyRelease = New-Object System.Runtime.InteropServices.Marshal.Release($pidyPointer)
# # END


# START
$procStrtr = New-Object System.Diagnostics.Process
$procStrtr.StartInfo.UseShellExecute = $false
$procStrtr.StartInfo.FileName = "/path/2/exebin"
$procStrtr.StartInfo.CreateNoWindow = $true
$procStrtr.Start()
# OR 
$proctor = [System.Diagnostics.Process]::Start('IExplore.exe', 'C:\\mal.html')
$proctor.Kill()
# ALSO
$p1dpr0c = [System.Diagnostics.Process]::GetProcessById(14369, '127.0.0.1')
$p1dpr0c | gm
$p1dpr0c.Handle # On Linux returned the PID

$nam3pr0c = [System.Diagnostics.Process]::GetProcessByName('chrome')
$nam3pr0c

$m3pr0c = [System.Diagnostics.Process]::GetCurrentProcess()
$m3pr0c

$a77pr0c = [System.Diagnostics.Process]::GetProcesses() # All processes, argv[1] "hostname" will get remote processes on hostame computer
$a77pr0c

Write-Host $proctor.ModuleName + " Base: " + $proctor.BaseAddress
Write-Host $proctor.ModuleName + " EntryPoint: " + $proctor.EntryPointAddress
Write-Host $proctor.ModuleName + " MemorySz: " + $proctor.ModuleMemorySize
Write-Host $proctor.ModuleName + " FileName: " + $proctor.FileName
# END


# START
# $memrye = New-Object System.IO
# END