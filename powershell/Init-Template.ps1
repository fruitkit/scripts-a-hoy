function Fx-Ex
{
<#
.SYNOPSIS
    Is a template for other scripts.
    Author: Daniel Faraimo
    License: BSD 3-Clause
    Required Dependencies: None
    Optional Dependencies: None
.DESCRIPTION
    Is a template for other scripts.
.PARAMETER ArgumentUno
    Is a pseudo-dummy parameter. String.
.PARAMETER ArgumentDos
    Is a pseudo-dummy parameter. Int32.
.EXAMPLE
    Fx-Ex -ArgumentUno 'Hello World'
    Description
    -----------
    Injest ArgumentUno.
.EXAMPLE
    "Hellow Mars" | Fx-Ex -ArgumentDos 69420
    Description
    -----------
    Injest ArgumentUno (pipe infered) and ArgumentDos (verbose).
.INPUTS
    System.Diagnostics.Process
    You can pipe a process object to Out-Minidump.
.OUTPUTS
    System.IO.FileInfo
.LINK
    https://fruitkit@bitbucket.org/fruitkit/scripts-a-hoy.git
#>

    [CmdletBinding()]
    Param (
        [Parameter(Position = 0, Mandatory = $True, ValueFromPipeline = $True)]
        # [System.Diagnostics.Process]
        [String]
        $ArgumentUno,

        [Parameter(Position = 1)]
        [ValidateScript({ Test-Path $_ })]
        [Int32]
        $ArgumentDos = 69420,
    )

    Write-Host $ArgumentUno
    Write-Host $ArgumentDos.toString()

}