
function Import-Visio {
    [CmdletBinding()]
    param (
        [Parameter(Mandatory = $true, Position = 0)]
        [Int32]
        $Sample
    )
    
    begin {
        Write-Output $null
    }
    
    process {

        # Visio DOM objects
        $visio = New-Object -ComObject Visio.Application

        # Create Visio Document
        $vdoc = $visio.Documents.Add(‘c:\temp\SampleVisio.vsdx’)
        $vpage1 = $vdoc.Pages(1)

        # Draw shapes
        $vpage1.DrawRectangle(1, 1, 5, 5)
        $vpage1.DrawOval(6, 6, 8, 8)

        # Open Visio Document
        $vdoc = $visio.Documents.Open('C:\temp\otherVisio.vsdx')
        $vpage = $visio.ActivePage



        $vpage.Shapes | ForEach-Object ($shape) 
        {
            # Find shapes without new lines
            if (!$shape.Text.Contains("`n")) {
                # Resolve DnsName with the $shape's provided hostname
                $address = Resolve-DnsName $shape.text 
                $address = $address| where-object {$_.IP4Address } | select-object -First 1
                $newLabel = "{0}`n{1}" -f $shape.text, $address.IP4Address
                $Shape.Text = $newLabel
            }
        }

        

        # Add icons
        $stencilPath='C:\Program Files (x86)\Microsoft Office\Office15\Visio Content\1033\SERVER_U.VSSX'
        $icons = $Visio.Documents.OpenEx($stencilPath, 64) # 64, openstencilhidden
        # $icons | Select-Object -Property Name
        $ftpSrv1 = $icons.Masters('FTP Server')
        $ftpSrv1.Drop($icons, 4, 4)

        $ftpSrv2 = $icons.Masters('FTP Server')
        $ftpSrv2.Drop($icons, 8, 8)

        # Connect the srv icons
        $ftpSrv1.AutoConnect($ftpSrv2, 0)

        # Select the connector and add arrows
        $ftpConnector = $visio.ActivePage.Shapes['Dynamic connector'] | Select-Object -Last 1
        $ftpConnector.Cells('EndArrow') = 4        	# 4 is the arrow type
        $ftpConnector.Cells('BeginArrow') = 4

    }
    
    end {
        Write-Output $null
    }
}
