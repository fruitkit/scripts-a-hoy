function Write-Process {
    [CmdletBinding()]
    param (
        [Parameter(Mandatory = $true, Position = 0)]
        [Byte[]]
        $ByteArray,

        [Parameter(Position = 1, Mandatory = $true)]
        [Int32]
        $ConsoleWidth,

        [Parameter(Position = 2, Mandatory = $true)]
        [Int32]
        $Offset
    )
    
    begin 
    {
        $Position = 0
        $Padding = ($ConsoleWidth * 2) + $ConsoleWidth
    }
    
    process
    {
        while ($Position -le ($Offset - 1)) 
        {
            $Line = ""
            $Line = "$($Position.ToString('X8'))h  " #0x00000000h
            $PrintBytes = ""
            $Text = ""

            foreach ($i in 0..($ConsoleWidth - 1)) {
                if ($Position -ge $Offset) {break}

                # toPrint <== ByteArray[Position]
                $PrintBytes += "$($ByteArray[$Position].ToString('X2')) "

                # If printable 'x', else print '.'
                if ( [Char]::IsLetterOrDigit($ByteArray[$Position]) -or [Char]::IsPunctuation($ByteArray[$Position]) -or [Char]::IsSymbol($ByteArray[$Position]) ) {$Text += [Char] $ByteArray[$Position]}
                else {$Text += '.'}

                $Position++
            }
            $Line += $PrintBytes.PadRight($Padding, ' ')
            $Line += " $Text"

            # Output Formatted Line
            $Line
        }
    }
    
    end {}
}

function Get-ProcessDump
{
    [CmdletBinding()] Param (
        [Parameter(Mandatory = $False, Position = 0, ValueFromPipeline = $True, ParameterSetName = "PipedProcess")]
        [System.Diagnostics.Process]
        $Process,

        [Parameter(Mandatory = $True, Position = 1)]
        [ValidateSet('PE', 'RAW')]
        [String]
        $OutputFormat,

        [Parameter(Mandatory = $False)]
        [ValidateScript( { Test-Path $_ })]
        [String]
        $SaveAs,

        # Start of a raw memory dump, 
        [Parameter(Mandatory = $False)]
        [System.IntPtr]
        $BaseAddress = [System.IntPtr]::Zero,

        # Offset of total bytes of memory to dump. If not specified default will dump all.
        [Parameter(Mandatory = $False)]
        [Int32]
        $Offset,

        # List specifics of a process
        [Parameter(Mandatory = $False)]
        [Switch]
        $Enumerate,

        # Dump Process Modules, TODO
        [Parameter(Mandatory = $False)]
        [Switch]
        $DumpModules,

        [Parameter(Mandatory = $False, ParameterSetName = "WriteConsole")]
        [Switch]
        $WriteConsole,

        [Parameter(Mandatory = $False, ParameterSetName = "WriteConsole")]
        [Int32]
        $ConsoleWidth = 16
    )
    BEGIN
    {
        if (-not (Get-WindowsErrorReporting)) {
            Enable-WindowsErrorReporting
        }
        
        $WER = [PSObject].Assembly.GetType('System.Management.Automation.WindowsErrorReporting')
        $WERNativeMethods = $WER.GetNestedType('NativeMethods', 'NonPublic')
        $Flags = [Reflection.BindingFlags] 'NonPublic, Static'
        $MiniDumpWriteDump = $WERNativeMethods.GetMethod('MiniDumpWriteDump', $Flags)
        $MiniDumpWithFullMemory = [UInt32] 2

        $ProcessFileName = "$($ProcessName)_$($ProcessId).dmp"
        $ProcessDumpPath = $DumpFilePath + $ProcessFileName
    }
    PROCESS
    {
        # Write Stdout/file of specified process
        if ($Process)
        {
            $ProcessId = $Process.Id
            $ProcessName = $Process.Name
            $ProcessHandle = $Process.Handle
            $ProcessMainModule = $Process.MainModule
            $ProcessBaseAddress = $ProcessMainModule.BaseAddress
            $ProcessVSize = $Process.VirtualMemorySize64 + 1

            [Byte[]] $ByteArray = New-Object Byte[]($ProcessVSize)

            if ($OutputFormat.ToUpper() -eq 'RAW') # Raw binary dump
            { 
                if ($SaveAs){
                    $FStream = New-Object IO.FileStream($ProcessDumpPath, [IO.FileMode]::Create)
                    $MiniDumpWriteDump.Invoke($null, @($ProcessHandle, $ProcessId, $FStream.SafeFileHandle, $MiniDumpWithFullMemory, [IntPtr]::Zero, [IntPtr]::Zero, [IntPtr]::Zero))
                    $FStream.Close()
                }
                else
                {
                    $MiniDumpWriteDump.Invoke($null, @($ProcessHandle, $ProcessId, $ByteArray, $MiniDumpWithFullMemory, [IntPtr]::Zero, [IntPtr]::Zero, [IntPtr]::Zero))
                    Write-Process -ByteArray $ByteArray -Offset $Offset -ConsoleWidth $ConsoleWidth
                }
            }
            else # PE binary dump
            {
                $BytesRead = 0
                $NewProcessIdHandle = [Winapi.Kernel32]::OpenProcess(([Winapi.Kernel32+ProcessAccessFlags]::PROCESS_VM_READ), 0, $ProcessId)
                [Winapi.Kernel32]::ReadProcessMemory($NewProcessIdHandle, $ProcessBaseAddress, $ByteArray, $ProcessVSize, $BytesRead) | Out-Null
                [Winapi.Kernel32]::CloseHandle($NewProcessIdHandle) | Out-Null

                if ($SaveAs)
                {
                    $FStream = New-Object IO.FileStream($ProcessDumpPath, [IO.FileMode]::Create)
                    $FStream.Write($ByteArray, 0, $Offset)
                    $FStream.Close()
                }
                else
                {
                    Write-Process -ByteArray $ByteArray -Offset $Offset -ConsoleWidth $ConsoleWidth
                }
            }
        } 
        else # Memory specific addresses dump
        {
            $ByteBlocks = 500Mb


            if ($Offset -gt $ByteBlocks + 1) 
            {
                $BytesRead = 0

                if ($SaveAs)
                {
                    $FStream = New-Object IO.FileStream($ProcessDumpPath, [IO.FileMode]::Create)
                    while ($BytesRead -le $Offset) 
                    {
                        [Byte[]] $ByteArray = New-Object Byte[]($ByteBlocks)
                        [System.Runtime.InteropServices.Marshal]::Copy($BaseAddress, $ByteArray, 0, $ByteBlocks)
                        $FStream.Write($ByteArray, 0, $ByteBlocks)
                        $BytesRead += $ByteBlocks
                    }
                    $FStream.Close()
                }
                else 
                {
                    while ($BytesRead -le $Offset) 
                    {
                        [Byte[]] $ByteArray = New-Object Byte[]($ByteBlocks)
                        [System.Runtime.InteropServices.Marshal]::Copy($BaseAddress, $ByteArray, 0, $ByteBlocks)
                        Write-Process -ByteArray $ByteArray -ConsoleWidth $ConsoleWidth -Offset $ByteBlocks
                        $BytesRead += $ByteBlocks
                    }
                }
            }
            else
            {
                if ($SaveAs) 
                {
                    [Byte[]] $ByteArray = New-Object Byte[]($Offset)
                    [System.Runtime.InteropServices.Marshal]::Copy($BaseAddress, $ByteArray, 0, $Offset)
                    $FStream = New-Object IO.FileStream($ProcessDumpPath, [IO.FileMode]::Create)
                    $FStream.Write($ByteArray, 0, $ByteBlocks)
                    $FStream.Close()
                }
                else
                {
                    [Byte[]] $ByteArray = New-Object Byte[]($ByteBlocks)
                    [System.Runtime.InteropServices.Marshal]::Copy($BaseAddress, $ByteArray, 0, $Offset)
                    Write-Process -ByteArray $ByteArray -ConsoleWidth $ConsoleWidth -Offset $Offset
                }
            }

        }
    }
}